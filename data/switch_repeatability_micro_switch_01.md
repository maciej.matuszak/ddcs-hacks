# Statistics for "data\switch_repeatability_micro_switch_01"

| Quantity     | X      | Y        | Z        | A        |
|--------------|--------|----------|----------|----------|
| Mean         | 26.2mm | 60.593mm | 0m       | 0m       |
| sdt dev      | 0m     | 0m       | 17.082um | 0m       |
| Min Error    | 0m     | 0m       | 1.25um   | 56.843fm |
| Max Error    | 0m     | 0m       | 71.25um  | 56.843fm |
| Mean Error   | 0m     | 0m       | 22.917um | 56.843fm |
| Median Error | 0m     | 0m       | 21.25um  | 56.843fm |