;#572 -t1 -s1"Fixed plate pos. MACH X" -s2"mm" -m9 -min=-999.000 -max=999.000
;#573 -t1 -s1"Fixed plate pos. MACH Y" -s2"mm" -m9 -min=-999.000 -max=999.000
;#574 -t1 -s1"Fixed plate probing start MACH Z" -s2"mm" -m9 -min=-999.000 -max=999.000

;#2055 -t1 -s1"Probe / M6 travel heigth MACH Z" -s2"mm" -m9 -min=-999.000 -max=999.000

;#2056 -t1 -s1"Manual tool change pos MACH X" -s2"mm" -m9 -min=-999.000 -max=999.000
;#2057 -t1 -s1"Manual tool change pos MACH Y" -s2"mm" -m9 -min=-999.000 -max=999.000
;#2058 -t1 -s1"Manual tool change pos MACH Z" -s2"mm" -m9 -min=-999.000 -max=999.000


; *********************************************
; * MAIN
; *********************************************
O000
(Pause M00 or M110 - display should still have comments from Tn M6)
M110(Switch repeatability test - press "SART")

(Move to MACH safe Z)
G90 G153 Z#2055 F#580

(Move to fixed plate X,Y)
G90 G153 X#572 Y#573 F#580

#1=0
M98 P111 L30;

(Move to safe Z)
G90 G153 Z#2055 F#580
M30
%

; *********************************************
; * Probe and record routine
; * #1 is start index
; *********************************************
O111

(MOVE to probing height start)
G90 G153 Z#574 F#580

(Probe)
M101
G91 G01 Z-100 F#2011
M102
G04 P0;Sync

RecordCoords[#1, #864, #865, #866, #867]
#1=#1+1
M99 (Exit O10001)
%