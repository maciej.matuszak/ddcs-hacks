# Statistics for "data\switch_repeatability_micro_switch_02"

| Quantity     | X      | Y        | Z        | A        |
|--------------|--------|----------|----------|----------|
| Mean         | 26.2mm | 60.593mm | 0m       | 0m       |
| sdt dev      | 0m     | 0m       | 1.5974um | 0m       |
| Min Error    | 0m     | 0m       | 666.67nm | 56.843fm |
| Max Error    | 0m     | 0m       | 8.1667um | 56.843fm |
| Mean Error   | 0m     | 0m       | 2.1222um | 56.843fm |
| Median Error | 0m     | 0m       | 1.8333um | 56.843fm |