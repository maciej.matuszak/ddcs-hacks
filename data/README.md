Data sets
=========

![switch_repeatability.jpg](./switch_repeatability.jpg)

# switch_repeatability_micro_switch_01

* manual spindle turn every probe
* mean error: 23um
* max error: 71um

# switch_repeatability_micro_switch_02

* no turning
* mean error: 2um
* max error: 8um