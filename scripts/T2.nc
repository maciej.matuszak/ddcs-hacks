

(T.nc - tool change macro)
;#2050 "Fixed Probe Plate MACH X"
;#2051 "Fixed Probe Plate MACH Y"
;#2052 "Fixed Probe Plate MACH Z"

;#2053 "Fixed Plate sensing distance"
;#2054 "Fixed Plate Zero Offset Z"

;#2055 "Manual M6 Tn Loc MACH X"
;#2056 "Manual M6 Tn Loc MACH X"
;#2057 "Manual M6 Tn Loc MACH X"


(1. Go to safe Z)
M98 P101
(2. go to X,Y,Z tool change location)
#1= #2053 - #864; calculate x offset
#2= #2054 - #865; calculate y offset
#3= #2055 - #866; calculate z offset
G91 ; we will use relative movements
G00 Z#3; first move to Z
G00 X#1 Y#2; then X and Y
(3. pause  - change the tool)
(should there be comment after M0 or the comment from M6 Tn will display)
M00

(4. go to X,Y above fixed plate)
M98 P101; go to safe Z
#1= #2055 - #864; calculate x offset
#2= #2056 - #865; calculate y offset
#3= #2057 - #866; calculate z offset
G91 ; we will use relative movements
G00 X#1 Y#2; first to X and Y
G00 Z#3; then move to Z



(5. touch the plate)
(6. calculate new Z)