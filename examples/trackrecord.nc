(Press pause key, then press start key, will exit the track record cycle)
#51=0
WHILE #110==0 DO1
WRRECODE[#51]
#51=#51+1
G04P-1;Please move to the next location
END1

(Play back the track you recorded before)
#52=0
WHILE #52<=#108 DO2
RDRECODE[#52]
G1X#104Y#105Z#106A#107
#52=#52+1
END2