(T.nc toolchnage j.p. 14.03.2019)
(IO assignment serial interface
(#1032 ATC hold/release)
(#1033 ATC fix)
(#1034 ATC clean air)


#1=18.60  ; X position first tool holder (in MACH koordinates)
#2=52.3   ; X distance of the toolholder
#3=-50    ; Y infront of the tool holder (start entry position)
#4=-8.56  ; Y of toolholder for get/release
#5=-10    ; Z top (for secure moves)
#6=-70    ; Z above tools
#7=-116   ; Z for release (a bit avove the tool holders)
#8=-124.5 ; Z for get (only move here if holdl/release #1032 is open
#9=0      ; current tool offset 
#10=0     ; new tool offset

;#592: Current Tool Nummer, 0=no tool, 1...10)
;#593: New Tool Nummer, 0=no tool, 1...10)

;#767+i: tool offset i-th tool
;#802 G53 Z coodinate

M05 (stop spindle)

IF [#592 EQ #593] GOTO3 

G90 G153 Z#5 F6000   ; move up
IF [#592>0] GOTO4
M110 (Make sure no tool in spindle!)
G04P0    ; disable look ahead
GOTO1    ; no current tool

N4 IF [#592>10] GOTO1   ; tool not from tool holder

(store old tool offset)
#9=#[767+#592]; current tool offset

(store old tool)
G90 G153 X[#1+[#592-1]*#2] Y#3 F6000; move above store position 
G90 G153 Z#7 F6000                  ; go down
G90 G153 Y#4 F2000                  ; move into store position

(open clamp, switch on air, wait, switch off air, close clamp)
G04P0    ; disable look ahead
#1032=1  ; open tool clamp
#1034=1  ; air on
G04P1500 (releasing tool)
G04P0    ; disable look ahead
#1032=0  ; end release tool
#1034=0  ; air off
G04P0    ; disable look ahead

G90 G153 Z#6 F6000; move somewhat above upper limit of tool holders

(get new tool)
N1 IF [#593<1] GOTO2 ; no new tool
IF [#593>10] GOTO2   ; tool does not exist

(store new tool offset)
#10=#[767+#593]; new tool offset

G90 G153 X[#1+[#593-1]*#2] Y#4 F6000; move above tool get position 
;Z is up, if no old tool or at #6 above tools

(open clamp, switch on air, move with open clamp into tool)
G04P0    ;disable look ahead
#1032=1  ;open tool clamp
#1034=1  ;air on
G04P0    ;disable look ahead
G90 G153 Z#7 F3000 ; move down
G90 G153 Z#8 F300 ; move tool into clamp
G04P0    ;disable look ahead
#1032=0  ;close clamp
#1034=0  ;air off
G04P500 (getting tool)
#1033=1  ;fix tool
G04P500 (fixing tool)
G04P0    ;disable look ahead
#1033=0  ;end fix tool
G90 G153 Z#7 F500; move a bit up
G90 G153 Y#3 F2000; move forward out of holder
N2 G90 G153 Z#5 F6000; move up

N3 G04P0; Synchronization
#802=#802-#9+#10; G53 Z correction for new tool height
#592=#593; after tool change actual tool number to new one