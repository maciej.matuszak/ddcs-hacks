O10050
#1651=1
G04P1000
#1651=0
M99

; M6 Command
O10006
IF#1405==0GOTO3 (if #905 param is O do not show manual dialog)

G0G53Z#1302  (go to #902 MACH safe Z)
G0G53X#1300Y#1301 (go to X,Y of G28 ref point - tool change position)
MarcoDialog "M6.rc"  (execute dialogue)
G43H#17 (activate selected tool offset)
IF#148==0GOTO3  (cancel???)

G0G53Z#102  (go to probing but what are 102..105)
G0G53X#103Y#104
G0G53Z#105

G91G31Z-1000L#682Q1K0F#106 ( Probing )

IF#1824==0GOTO1

#1560=0
#[1560+#1824]=#1502-#108
#[764+#1824-1]=#[1560+#1824]
#1560=1
#[1560+#1824]=0
#[780+#1824-1]=0

GOTO2
N1#1556=#1502
#108=#1502
N2G0G53Z#102
N3
M99

O20000
G43H#17
M99

