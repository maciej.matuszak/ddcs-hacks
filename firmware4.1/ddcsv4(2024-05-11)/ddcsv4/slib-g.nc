(G12 I)
O9012
G91 G01 X#6
G02 X0 I-#6
G01 X-#6
M99

(G13 I)
O9013
G91 G01 X#6
G03 X0 I-#6
G01 X-#6
M99

O9100
X#50Y#51
M99

(G102 oblique ellipse X length Y width A angle between the major axis of the oblique ellipse and the X axis B initial angle C end angle L angle step)
O9102
#60=-#4
N1 #61=#0*COS[#60]
#62=#1*SIN[#60]
#63=#61*COS[#3]-#62*SIN[#3]
#64=#61*SIN[#3]+#62*COS[#3]
G1X#63+#44Y#64+#45
#60=#60-#10
IF ABS[#60]<=#5 GOTO1
G1X#44Y#45
M99

(G103逆斜椭圆 X长 Y宽 A斜椭圆长轴与X轴角度 B初始角度 C终止角度 L角度步长)
O9103
#60=#4
N1 #61=#0*COS[#60]
#62=#1*SIN[#60]
#63=#61*COS[#3]-#62*SIN[#3]
#64=#61*SIN[#3]+#62*COS[#3]
G1X#63+#44Y#64+#45
#60=#60+#10
IF #60<=#5 GOTO1
G1X#44Y#45
M99
