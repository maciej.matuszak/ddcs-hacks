O10000
G04P-1
M99

O10100
M0
M99

O10030
#1802=1
M5M9M11
#1802=0
M99

O10047
M99

O8000(回暂停点)
(全局参数#1402:Z轴安全高度)
(全局参数#536:A轴循环编码器使能)
(全局参数#1401:Z轴是否回安全高度)
#20=#0-#1500
#21=#1-#1501
#23=#3-#1503
#24=#4-#1504

IF#536==0GOTO10
#13=#3-#1503
#23=#13-360*FIX[#13/360]
IF#23>180GOTO11
IF#23<-180GOTO12
GOTO10
N11 #23=#23-360
GOTO10
N12 #23=#23+360
N10

IF#1401==1GOTO20
#22=#2-#1502
IF#22>0GOTO21
G91G01X#20Y#21
A#23B#23
Z#22
GOTO3
N21
G91G01Z#22
X#20Y#21
A#23B#23
GOTO3

N20
#10=#1402
IF#1502>=#10GOTO1
G91G0G53Z#10
N1
IF#2-#1502>0GOTO2
G91G01X#20Y#21
A#23B#23
Z#2-#1502
GOTO3
N2
G91G01Z#2-#1502
X#20Y#21
A#23B#23
N3
M99

O8001
#495=-1
IF#1==505GOTO1
IF#1==342GOTO2
IF#1==585GOTO3
IF#1==586GOTO4
IF#1==587GOTO5
MarcoDialog "advstart.rc"
GOTO6
N1 MarcoDialog "advstart-array.rc"
GOTO6
N2 MarcoDialog "advstart-sr.rc"
GOTO6
N3 MarcoDialog "break.rc"
GOTO6
N4 MarcoDialog "break-array.rc"
GOTO6
N5 MarcoDialog "break-sr.rc"
N6 #495=0
M99

O8002
(全局参数#1402:Z轴安全高度)
(全局参数#1400:Z轴是否回安全高度)
IF#1400==0GOTO1
G00Z#1402
N1

(Global parameter #496: The number of remaining workpieces is initialized to 0 when array processing is initiated)
(Global parameter #499: Cycle processing flag, if it is 1, the system program will call this nc program again after the execution of the nc program is completed.)
IF#496==-2GOTO2
IF#496==-3GOTO3
GOTO4

(Start array processing flow)
N2 #499=0
MarcoDialog "array.rc"
#499=1
GOTO10

(Start the typesetting process)
N3 #499=0
MarcoDialog "template.rc"
#499=1
GOTO10

(array unit processing flow)
N4 #499=1  
#496=#496+1
G52X#0Y#1Z#2A#3B#3
G0X#6Y#7

(记录阵列启动时机床坐标)
#490=#1500
#491=#1501
#492=#1502
#493=#1503
#494=#1504
#495=#1505
N10
M99

O8003
#0=0
#1=0
#12=#123
#13=#124
#14=#125
#15=#126
#16=#127
#17=#128
#18=#129
MarcoDialog "simulate.rc"
#0=1
#123=#12
#124=#13
#125=#14
#126=#15
#127=#16
#128=#17
#129=#18
N1
M99

O9028
G91X#0Y#1Z#2A#3B#4C#5
G#12X#6Y#7Z#8A#9B#10C#11
M99

O9030
G91X#0Y#1Z#2A#3B#4C#5
G#12X#6Y#7Z#8A#9B#10C#11
M99

O9053
G91X#0Y#1Z#2A#3B#4C#5
M99

O9081
IF #13>0 GOTO1
#13=1
(Loop processing)
N1WHILE#13>0DO13
G91G00X#0Y#1A#3B#3C#3;XYAB快速进给到孔位处
G90G00Z#10;Z快速进给到R点
G90G01Z#11;Z以切削速度钻至孔底位置
G90G00Z#10;Z快速抬升到R位置
#13=#13-1;循环次数递减
END13
G90G00Z#12;Z快速抬升到退刀位置
M99

O9082
IF #13>0 GOTO1
#13=1
(Loop processing)
N1WHILE#13>0DO13
G91G00X#0Y#1A#3B#3C#3;XYAB快速进给到孔位处
G90G00Z#10;Z快速进给到R点
G90G01Z#11;Z以切削速度钻至孔底位置
G04P#14
G90G00Z#10;Z快速抬升到R位置
#13=#13-1;循环次数递减
END13
G90G00Z#12;Z快速抬升到退刀位置
M99

O9073
IF #13>0 GOTO5
#13=1
(循环处理)
N5 WHILE#13>0DO13
#9=#10
G91G00X#0Y#1A#3B#3C#3;XY快速进给到孔位处
G90G00Z#10;ZRapid traverse to point R
N6#9=#9-ABS[#15];计算逐层钻孔位置
#20=#9+ABS[#16];计算钻孔回退位置
IF #9>#11 GOTO7
#9=#11
#20=#10
N7G90G01Z#9;Z以切削速度逐层钻孔
G90G00Z#20;Z轴逐层钻孔间歇回退
IF #9==#11 GOTO8
GOTO6
N8#13=#13-1;循环次数递减
END13
G90G00Z#12;Z快速抬升到退刀位置
M99

O9083
IF #13>0 GOTO5
#13=1
(循环处理)
N5 WHILE#13>0DO13
#9=#10
G91G00X#0Y#1A#3B#3C#3;XY快速进给到孔位处
G90G00Z#10;Z rapid traverse to point R
N6#9=#9-ABS[#15];Calculate drilling locations layer by layer
#20=#9+ABS[#16];Calculate drilling retraction position
IF #9>#11 GOTO7
#9=#11
#20=#10
N7G90G01Z#9;Z drills layer by layer at cutting speed
G90G00Z#10;Z轴回退至R点
G90G00Z#20
IF #9==#11 GOTO8
GOTO6
N8#13=#13-1;循环次数递减
END13
G90G00Z#12;Z快速抬升到退刀位置
M99

O9074
#1999=0;Set left-hand tapping thread
IF #13>0 GOTO1
#13=1
(循环处理)
N1WHILE#13>0DO13
G91G00X#0Y#1A#3B#3C#3;XYAB快速进给到孔位处
G90G00Z#10;Z快速进给到R点
G90G01Z#11;以切削速度至孔底位置,伺服主轴反转
G90G01Z#10;以切削速度抬升至R点,伺服主轴正转
#13=#13-1;循环次数递减
END13
G90G00Z#12;Z快速抬升到退刀位置
M99

O9084
#1999=1;Set right-hand tapping thread
IF #13>0 GOTO1
#13=1
(循环处理)
N1WHILE#13>0DO13
G91G00X#0Y#1A#3B#3C#3;XYAB快速进给到孔位处
G90G00Z#10;Z快速进给到R点
G90G01Z#11;以切削速度至孔底位置,伺服主轴正转
G90G01Z#10;以切削速度抬升至R点,伺服主轴反转
#13=#13-1;循环次数递减
END13
G90G00Z#12;Z快速抬升到退刀位置
M99

