# DDCS CNC controllers user notes

[[_TOC_]]

# Firmware Version

* To check current version check home screen
* Update:
    * Format SD card as FAT32
    * copy install folder to it.
    * Power up DDCS with SD plugged in
    * Should come up with instructions??? - never notice that

# Debugging

* Use `RecordCoords[n, x, y, z, a]` to store the results in `ProbeMap{n}.txt` in root of SD card
* `ClearCoords[n]` to clear the file
* Print to screen and pause `M110(comment comment)`

# Settings backup

Settings can be accessed in `/mnt/nand1-1/settings`. You can use it to copy/paste to usb stick: `/udisk-sda1/`.
See `4.2 Save the Parameters Setting` in `docs/DDCS V31 MANUAL V3.pdf`

# Postprocessor

* FreeCAD "refactored_mach3_mach4" with --no-tlo
    * still experimenting with it
    * there may be DDCS one, but it seems it is mach3 with no tlo and problematic
      operations: https://forum.freecad.org/viewtopic.php?p=660840#p660840

# [Probing](./docs/probing.md)

# User defined subroutines

source: https://webcache.googleusercontent.com/search?q=cache:qQO7bK6EcqwJ:bbs.ddcnc.com/forum.php%3Fmod%3Dviewthread%26tid%3D7&cd=10&hl=en&ct=clnk&gl=au
DDCSV system supports macros. Macros support loops, conditional control languages, and common maths function calls.
For user processing files whose size is less than 100K or whose initial character name is sys, the system automatically
scans the program structure before processing or emulation so as to implement the loop and branch statement support.
Note: The main program number of macro processing program defaults to 0.

Example:
Processing file: sysuser1.nc

```
O0000 (main entrance)
...
M98P0001L3 (call subroutine No.1 three times)
...
M30 ; end of main program

O0001 ; subroutine start
...
...
M99 ; subroutine exit
```

# Misc

102 subprogram is used to play back the track of the tool. Before using the 102 subroutine, the track must be recorded, the attachment is an example related to it.
```
(Press pause key, then press start key, will exit the track record cycle)
#51=0
WHILE #110==0 DO1
WRRECODE[#51]
#51=#51+1
G04P-1;Please move to the next location
END1

(Play back the track you recorded before)
#52=0
WHILE #52<=#108 DO2
RDRECODE[#52]
G1X#104Y#105Z#106A#107
#52=#52+1
END2
```

# Actions technical descriptions
## Zero Axes
This changes the GXX->G53 offset

## Tool length adjustment
During probing and tool change operation the G53->MACH Z offset is adjusted 