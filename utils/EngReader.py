from argparse import ArgumentParser
from logging import getLogger, Logger
from pathlib import Path

from utils.ParamType import ParamType
from utils.ParameterDecoder import ParameterDecoder
from utils.ParameterDecoderException import ParameterDecoderException
from utils.SettingsReader import SettingsReader


class EngReader:
    def __init__(self, in_file: Path):
        self.lg: Logger = getLogger(self.__class__.__name__)
        self.in_file: Path = in_file

    def readParams(self):
        line_no = 1
        with self.in_file.open(mode='r', encoding='UTF-8') as f:
            while line := f.readline():
                try:
                    param = ParameterDecoder.decode(line_no, line)
                    yield param
                except ParameterDecoderException as ex:
                    self.lg.error(f'Failed to parse Parameter; line: {line_no}: {line}', exc_info=ex)
                finally:
                    line_no += 1


def main():
    parser = ArgumentParser()
    parser.add_argument('eng_file', help='Input Language file')
    parser.add_argument('settings_file', help='Input Settings file')
    args = parser.parse_args()
    eng_file = Path(args.eng_file)
    settings_file = Path(args.settings_file)
    reader = EngReader(eng_file)
    params = {}
    for param in reader.readParams():
        if param:
            params[param.pid] = param
    settings_reader = SettingsReader(settings_file, params)
    settings_reader.readSettings()

    export_file = settings_file.parent / f'{settings_file.stem}.txt'
    with export_file.open('w', encoding='UTF-8') as f:
        for p_id, pv in settings_reader.values.items():
            if pv.ptype == ParamType.CHOICE:
                if pv.value in pv.enums:
                    p_value = f'{pv.enums[pv.value]}({pv.value})'
                else:
                    p_value = f'Invalid value: {pv.value} for choices: {pv.enums}; data: 0x{pv.data.hex().upper()}'
            else:
                p_value = pv.value

            if pv.value == 0 and len(pv.name) == 0:
                continue

            print(f'#{p_id:03}: ({pv.ptype.name}) = {p_value}  - {pv.name}')
            f.write(f'#{p_id:03}: ({pv.ptype.name}) = {p_value}  - {pv.name}\n')


if __name__ == '__main__':
    main()
