import struct


class IpAddress:
    def __init__(self, data: bytes):
        self.numbers = list(struct.unpack('8B', data))
        pass

    def __str__(self):
        numbers_str = [str(n) for n in self.numbers]
        return f'IpAddress({".".join(numbers_str[:4])}) '

    def __repr__(self):
        return self.__str__()