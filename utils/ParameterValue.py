from utils.Parameter import Parameter


class ParameterValue(Parameter):
    def __init__(self, param: Parameter):
        if param:
            pd = param.__dict__
        else:
            pd = {}

        super().__init__(**pd)
        self.value = None
        self.data = None
