import os
import re
from argparse import ArgumentParser
from io import TextIOWrapper
from pathlib import Path
from typing import List, Optional

G1ZF_LINE = re.compile(r'^G1\s+Z(?P<z_value>[+-]*[\d.]+)\s+F(?P<f_value>[+-]*[\d.]+)$')
G0XY_LINE = re.compile(r'^G0\s+X(?P<x_value>[+-]*[\d.]+)\s+Y(?P<y_value>[+-]*[\d.]+)$')
ZF_LINE = re.compile(r'^Z(?P<z_value>[+-]*[\d.]+)\s+F(?P<f_value>[+-]*[\d.]+)$')
XY_LINE = re.compile(r'^X(?P<x_value>[+-]*[\d.]+)\s+Y(?P<y_value>[+-]*[\d.]+)$')
Z_LINE = re.compile(r'^Z(?P<z_value>[+-]*[\d.]+)$')
M9_LINE = re.compile(r'^M9$')
M5_LINE = re.compile(r'^M5$')
G28G91Z0_LINE = re.compile(r'^G28\s+G91\s+Z(?P<z_value>[+-]*[\d.]+)$')


class LineBufferLine:

    def __init__(self, line: str):

        self._line = line
        self._new_line: Optional[str] = None
        self.is_g1zf = False
        self.is_g0xy = False
        self.is_zf = False
        self.is_xy = False
        self.is_z = False
        self.is_g28g91z0 = False
        self.is_m9 = False
        self.is_m5 = False
        self.is_empty = False

        self.X: Optional[float] = None
        self.Y: Optional[float] = None
        self.Z: Optional[float] = None
        self.F: Optional[float] = None

        if len(line.strip()) == 0:
            self.is_empty = True
        else:
            self.parse_line(line)

    @property
    def line(self) -> str:
        return self._new_line or self._line

    @line.setter
    def line(self, value: str):

        # remove trailing '0'
        value = re.sub(r'(\d+\.[1-9]*)(0+)(\s)', r'\1\3', value)
        self._new_line = value

    def parse_line(self, line):
        try:
            res = G1ZF_LINE.search(line)
            if res:
                self.is_g1zf = True
                self.Z = float(res.group('z_value'))
                self.F = float(res.group('f_value'))
                return

            res = G0XY_LINE.search(line)
            if res:
                self.is_g0xy = True
                self.X = float(res.group('x_value'))
                self.Y = float(res.group('y_value'))
                return

            res = ZF_LINE.search(line)
            if res:
                self.is_zf = True
                self.Z = float(res.group('z_value'))
                self.F = float(res.group('f_value'))
                return

            res = XY_LINE.search(line)
            if res:
                self.is_xy = True
                self.X = float(res.group('x_value'))
                self.Y = float(res.group('y_value'))
                return

            res = Z_LINE.search(line)
            if res:
                self.is_z = True
                self.Z = float(res.group('z_value'))
                return

            res = M9_LINE.search(line)
            if res:
                self.is_m9 = True
                return

            res = M5_LINE.search(line)
            if res:
                self.is_m5 = True
                return

            res = G28G91Z0_LINE.search(line)
            if res:
                self.is_g28g91z0 = True
                self.Z = float(res.group('z_value'))
                return
        except Exception as e:
            pass

    def __str__(self):
        return self.line

    def __repr__(self):
        return self.line


class LineBuffer:

    def __init__(self, size: int, out_file: Path):
        self.data: List[LineBufferLine] = []
        self.out_file = out_file
        self.size = size
        self.output: Optional[TextIOWrapper] = None

    def push_line(self, line: str) -> LineBufferLine:
        ret_lbl = LineBufferLine(line)
        self.data.append(ret_lbl)
        while len(self.data) > self.size:
            lbl = self.data.pop(0)

            self.output.write(lbl.line)

        return ret_lbl

    def flush(self):
        while self.data:
            lbl = self.data.pop(0)
            self.output.write(lbl.line)

    def __enter__(self):
        self.output = self.out_file.open(mode='w')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.flush()
        self.output.close()


class GCodeParse:

    def __init__(self, in_file: Path, output_folder: Path):
        self.work_f = None
        self.in_file = in_file
        self.out_file = output_folder / in_file.name

    def run(self):
        with self.in_file.open(mode='r') as in_file:
            with LineBuffer(5, self.out_file) as lb:
                line_no = 0
                while line := in_file.readline():
                    line_no += 1
                    lb.push_line(line)
                    if len(lb.data) < 5:
                        continue

                    lbl5 = lb.data[-5]
                    lbl4 = lb.data[-4]
                    lbl3 = lb.data[-3]
                    lbl2 = lb.data[-2]
                    lbl1 = lb.data[-1]

                    add
                    states:
                    Idle
                    HAs
                    work_F
                    tick, tick, tick
                    final
                    change

                if lbl1.is_z and lbl2.is_g1zf and lbl4.is_g0xy:
                    self.work_f = lbl2.F
                    lbl1.line = f'G1 Z{lbl1.Z:.3f} F{self.work_f:.3f}{os.linesep}'
                    lbl2.line = f'G0 Z{lbl2.Z:.3f}{os.linesep}'

                elif lbl1.is_zf and lbl2.is_g1zf and lbl4.is_g0xy:
                    self.work_f = lbl2.F
                    lbl1.line = f'G1 Z{lbl1.Z:.3f} F{lbl1.F:.3f}{os.linesep}'
                    lbl2.line = f'G0 Z{lbl2.Z:.3f}{os.linesep}'

                elif lbl1.is_g28g91z0 and lbl2.is_m5 and lbl3.is_m9 and lbl4.is_empty and lbl5.is_z:
                    lbl5.line = f'G0 Z{lbl5.Z:.3f}{os.linesep}'

                elif lbl1.is_z and lbl2.is_z and lbl3.is_xy and lbl4.is_z:
                    lbl1.line = f'G1 Z{lbl1.Z:.3f} F{self.work_f:.3f}{os.linesep}'
                    lbl4.line = f'G0 Z{lbl4.Z:.3f}{os.linesep}'
                elif lbl1.is_z and lbl2.is_xy and lbl3.is_z and lbl3.Z > lbl1.Z:
                    lbl3.line = f'G0 Z{lbl3.Z:.3f}{os.linesep}'
                    pass


def main():
    parser = ArgumentParser("GCodeParser")
    parser.add_argument('input_file', type=Path, help='Input file')
    parser.add_argument('out_folder', type=Path, help='Output folder')
    args = parser.parse_args()

    if not args.out_folder.exists():
        args.out_folder.mkdir(parents=True)
    g_parser = GCodeParse(args.input_file, args.out_folder)
    g_parser.run()


if __name__ == '__main__':
    main()
