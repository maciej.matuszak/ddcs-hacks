import struct
from logging import Logger, getLogger
from pathlib import Path
from typing import Dict, Any

from utils.ParamType import ParamType
from utils.Parameter import Parameter
from utils.ParameterValue import ParameterValue


class SettingsReader:
    def __init__(self, in_file: Path, params: Dict[int, Parameter]):

        self.lg: Logger = getLogger(self.__class__.__name__)
        self.in_file: Path = in_file
        self.params = params
        self.values: Dict[int, ParameterValue] = {}

    def readSettings(self):
        param_id = 0
        with self.in_file.open(mode='rb', ) as f:
            while data := f.read(8):
                try:
                    param = self.params.get(param_id, None)
                    if param is None:
                        p_type = ParamType.UNKNOWN
                    else:
                        p_type = param.ptype
                    pv = ParameterValue(param)
                    value = struct.unpack(p_type.decode_format, data)[0]
                    value = p_type.type(value)
                    pv.data = data
                    pv.value = value
                    self.values[param_id] = pv

                except Exception as ex:
                    self.lg.error(f'Failed to parse settings; line: {param_id}, data:{data}, type:{p_type}', exc_info=ex)
                finally:
                    param_id += 1
