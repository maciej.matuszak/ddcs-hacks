from typing import Union, Dict

from utils.ParamType import ParamType


class Parameter:

    def __init__(self, pid: int = -1,
                 group: str = '',
                 ptype: ParamType = ParamType.UNKNOWN,
                 name: str = '',
                 unit: str = '',
                 pmin: Union[int, float, None] = None,
                 pmax: Union[int, float, None] = None,
                 enums: Dict[int, str] = None,
                 ppp: str = '',
                 description: str = '') -> None:
        self.pid = pid
        self.group = group
        self.ptype = ptype
        self.name = name
        self.unit = unit
        self.pmin = pmin
        self.pmax = pmax
        self.enums = enums
        self.ppp = ppp
        self.description = description

    def __str__(self):
        return f'#{self.pid} - {self.name}'

    def __repr__(self):
        return self.__str__()
