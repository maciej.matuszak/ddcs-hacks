import re
from typing import Optional

from utils.ParamType import ParamType
from utils.Parameter import Parameter
from utils.ParameterDecoderException import ParameterDecoderException

PAR_ID_MATCHER = re.compile(r'^#(\d+)\s')
PAR_TYPE_MATCHER = re.compile(r'-t(\d+)')
PAR_NAME_MATCHER = re.compile(r'-s1\s*"(.*?)"')
PAR_UNITS_MATCHER = re.compile(r'-s2\s*"(.*?)"')
PAR_DESCRIPTION_MATCHER = re.compile(r'-s3\s*"(.*?)"')
PAR_GROUP_MATCHER = re.compile(r'-m(\d+)')
PAR_PPP_MATCHER = re.compile(r'-p(\d+)')
PAR_MIN_MATCHER = re.compile(r'-min=([+-]*[\d\.]+)')
PAR_MAX_MATCHER = re.compile(r'-max=([+-]*[\d\.]+)')
PAR_ENUM_MATCHER = re.compile(r'-i(\d+)\"(.*?)\"')


def get_matcher_first_value(line, matcher):
    m = matcher.search(line)
    if m is None:
        return None, line
    line = line[:m.start()] + line[m.end():]

    return m.group(1), line


class ParameterDecoder:

    @classmethod
    def decode(cls, line_no: int, line: str) -> Optional[Parameter]:
        p_id, line = get_matcher_first_value(line, PAR_ID_MATCHER)
        if p_id is None:
            return None
        p_id = int(p_id)

        p_type, line = get_matcher_first_value(line, PAR_TYPE_MATCHER)
        if p_type:
            p_type = ParamType(p_type)
        else:
            p_type = ParamType.UNKNOWN
        p_name, line = get_matcher_first_value(line, PAR_NAME_MATCHER)
        p_units, line = get_matcher_first_value(line, PAR_UNITS_MATCHER)
        p_description, line = get_matcher_first_value(line, PAR_DESCRIPTION_MATCHER)
        p_ppp, line = get_matcher_first_value(line, PAR_PPP_MATCHER)
        p_group, line = get_matcher_first_value(line, PAR_GROUP_MATCHER)
        p_min, line = get_matcher_first_value(line, PAR_MIN_MATCHER)
        if p_min:
            p_min = p_type.type(float(p_min))

        p_max, line = get_matcher_first_value(line, PAR_MAX_MATCHER)
        if p_max:
            p_max = p_type.type(float(p_max))

        enums = {}
        while m := PAR_ENUM_MATCHER.search(line):
            assert p_type == ParamType.CHOICE
            k = m.group(1)
            k = p_type.type(k)
            v = m.group(2)
            enums[k] = v
            line = line[:m.start()] + line[m.end():]
        line = line.strip()
        if line:
            raise ParameterDecoderException(f'Line {line_no}: has un-decoded leftovers: {line}')

        param = Parameter(pid=p_id, group=p_group, ptype=p_type, name=p_name, unit=p_units, pmin=p_min, pmax=p_max,
                          enums=enums, ppp=p_ppp, description=p_description)
        return param
