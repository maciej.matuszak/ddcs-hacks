"""ddcs_stats

Usage:
  ddcs_stats.py <data_folder>
  ddcs_stats.py (-h | --help)
  ddcs_stats.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
"""
from pathlib import Path

import numpy as np
import logging as log

from docopt import docopt
from numpy.typing import NDArray
from engfmt import Quantity
from tabulate import tabulate


def read_ddcs_data(data_folder: Path) -> NDArray:
    result: NDArray = None
    for item in data_folder.glob(r'ProbeMap*.txt'):
        item_data: NDArray = np.genfromtxt(item, delimiter=",")
        if result is None:
            result = item_data.reshape(1, 4)
        else:
            result = np.append(result, item_data.reshape(1, 4), axis=0)
    return result


def main():
    log.basicConfig(level=log.INFO)
    arguments = docopt(__doc__, version='ddcs_stats 1.0')

    data_folder = Path(arguments['<data_folder>'])
    data = read_ddcs_data(data_folder) * 0.001
    means = np.mean(data, axis=0)
    errors = np.abs(data - means)

    def formatter(x):
        if x < 1e-15:
            x = 0.0
        return Quantity(x, 'm').to_eng()

    means_q = [formatter(x) for x in means]
    std_dev = [formatter(x) for x in np.std(errors, axis=0)]

    min_errors = [formatter(x) for x in np.min(errors, axis=0)]
    max_errors = [formatter(x) for x in np.max(errors, axis=0)]
    mean_errors = [formatter(x) for x in np.mean(errors, axis=0)]
    median_errors = [formatter(x) for x in np.median(errors, axis=0)]

    headers = ['Quantity', 'X', 'Y', 'Z', 'A']
    tab_data = []
    tab_data += [['Mean'] + means_q]
    tab_data += [['sdt dev'] + std_dev]

    tab_data += [['Min Error'] + min_errors]
    tab_data += [['Max Error'] + max_errors]
    tab_data += [['Mean Error'] + mean_errors]
    tab_data += [['Median Error'] + median_errors]

    print(tabulate(tab_data, headers, tablefmt="github"))
    report_file = data_folder.parent / f'{data_folder.stem}.md'

    with report_file.open(mode='w') as f:
        f.write(f'# Statistics for "{data_folder}"\n\n')
        f.write(tabulate(tab_data, headers, tablefmt="github"))


if __name__ == '__main__':
    main()
