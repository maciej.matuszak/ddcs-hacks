from enum import Enum
from typing import Type

from utils.IpAddress import IpAddress


class ParamType(Enum):
    INT = '0', int, '<q'
    REAL = '1', float, '<d'
    CHOICE = '2', int, '<q'
    IP_ADDRESS = '3', IpAddress, '8s'
    UNKNOWN = '', int, '<q'

    def __new__(cls, *args, **kwds):
        obj = object.__new__(cls)
        obj._value_ = args[0]
        return obj

    # ignore the first param since it's already set by __new__
    def __init__(self, _: str, type_: Type, decode_format: str):
        self._type = type_
        self._decode_format = decode_format

    def __str__(self):
        return f'{self.value} - {self.name}'

    def __repr__(self):
        return self.__str__()

    # this makes sure that the description is read-only
    @property
    def type(self) -> Type:
        return self._type

    @property
    def decode_format(self) -> str:
        return self._decode_format
