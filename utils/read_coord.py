import array
from argparse import ArgumentParser
from pathlib import Path

import numpy as np
from tabulate import tabulate


def main():
    parser = ArgumentParser()
    parser.add_argument('coord1_file', help='coord1 file')
    arg = parser.parse_args()
    coord1_file = Path(arg.coord1_file)

    with (coord1_file.open(mode='rb') as f):
        data = f.read()
        n = int(len(data) // 8)
        formatter = f'{n}<d'
        numbers = array.array('d', data)
        offsets = [[0] * 7 for _ in range(9)]
        v_headers = ['Unknown',
                     'G54 Offset',
                     'G55 Offset',
                     'G56 Offset',
                     'G57 Offset',
                     'G58 Offset',
                     'G59 Offset',
                     'Unknown',
                     'G53 Offset',
                     ]
        for row_idx in range(len(offsets)):
            row = offsets[row_idx]
            row[0] = v_headers[row_idx]
            for col_idx in range(6):
                value = numbers[row_idx * 6 + col_idx]
                row[col_idx + 1] = value

        text = tabulate(offsets,
                        headers=['X', 'Y', 'Z', 'A', 'B', 'C'],
                        tablefmt='github',
                        floatfmt=".3f")
        outFile = coord1_file.parent / f'{coord1_file.stem}.txt'
        with open(outFile, 'w') as f_out:
            f_out.write(text)



if __name__ == '__main__':
    main()
