# Assumptions
* Probe in mode1
* using fixed sensor plate
* Origin is in corner of stock suitable for XYZ probe

# Procedure/workflow
1. note the Origin in your CAM or setup sheet
2. us XYZ probe to find origin corner location
3. move probe to X=0,Y=0,Z=0
4. Zero All axes - this will set the `#69`=0
5. execute PROBE - when `#69`==0 it will just record offset of fixed sensor plate and update `#69`. Any subsequent PROBE will adjust the `G53`->`MACH` Z offset
6. RUN the G-code
    * any `M6` will end in PROBE procedure and `G53`->`MACH` offset will be updated
    * if you "Zero All Axes" again you will loose the `#69` and will have to start from scratch 