General notes
=============
![X6-1500GT.jpg](./X6-1500GT.jpg)

* Spindle 1.5kW 8000..24000 rpm
* X,Y axes reasonably rigid
* Z axis is piece of crap need upgrade

Aluminium
---------

* min rpm tested on aluminium 16200
* Using mist with minimum of Methylated alcohol
* chip load:
    * tool > 1.587 mm use, min 0.0254 mm/tooth to prevent rubbing - tool supplier specs usually are designed for very
      rigid machine
    * tool < 1.587 mm use micro settings - tool supplier specs, rigidity is not a problem (except Z axis problems)

| Operation | Tool        | DOC [mm]      | WOC [mm]     | Dir [CO/CL] | s [rpm] | F mm/min               | Comment                                                                                        |
|-----------|-------------|---------------|--------------|-------------|---------|------------------------|------------------------------------------------------------------------------------------------|
| Facing    | D6 F2 Helix | 0.25          | 68%          | CO          | 16.2k   | 1200 rough, 640 finish | Run along Y axis has much better finish due to excessive vibration (Z axis lin. rail is loose) |
| Adaptive  | D6 F1 Helix | 0.508 - 0.762 | 5.84         | CO          | 24k     | 1905                   | Run shallow and wide WOC 70..100%                                                              |
| Contour   | D6 F2 Helix | 10            | 0.25         |             | 16.2k   | 1000                   | along Y was ok, X not so much same problem as described in facing                              |
| Adaptive  | D2 F1 Helix | 0.3           | F360 Auto??? | CO          | 24k     | 260                    | Ensure reasonable ramp/helix entry F360 defaults cut lot of air                                |
| Contour   | D2 F1 Helix | 0.3           | 0.25         | CO          | 24k     | 200                    |                                                                                                |

Wood
----
TBD...