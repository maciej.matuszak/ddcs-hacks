# Aluminium Cutting Receipes

## Type: 2D Adaptive Clearin

Source: https://www.youtube.com/watch?v=Q4n5MqpX-Ms
Material: 6061-T6
Cutter: 4mm 1F CBR
DOC: 158% 6.35mm
WOC: 37.5% 1.5mm

Speed: 21k rpm
Fees: 1500 mm/m

Notes: chips evacuating trough bottom as well - too fasat otherwise

## Type: 2D Adaptive Pocketing

Source: https://www.youtube.com/watch?v=3gITJN9NlKU&list=PLsiEJAZgdF8Jz60HfNvy415mHReZtNI0I&index=5
Material: 6061-T6
Cutter: 4mm 1F CBR (Huhao premium, very short)
DOC: 158% 6.35mm
WOC: 38% 1.52mm
Speed: 24k rpm
Feed: 1778 mm/m

Contour (slotting):
DOC: 1.57mm
WOC: 4mm 100%
Feed: 1524 mm/min

Notes: seems possible

## Type: Slotting

Source: https://www.youtube.com/watch?v=rzy-HhlptWc&list=PLsiEJAZgdF8Jz60HfNvy415mHReZtNI0I&index=6
Material: 6061-T6
Cutter: 6mm 1F CBR (Huhao premium, very short)
DOC: 50% 3mm
WOC: 100% 6mm
Speed: 24k rpm
Feed: 3683..4064 mm/min

Source: PLsiEJAZgdF8Jz60HfNvy415mHReZtNI0I
Endmill: 4mm Huhao Single Flute (for acrylic only, yyeah right!)
Feed: 2000mm/min (78IPM)
DOC: 1.8mm
WOC: 3.8mm

TBD: sound promissing:
https://www.youtube.com/watch?v=68r-1cDio40

## JBWORKS

Ap = DOC
Ae = WOC
Source: https://www.youtube.com/watch?v=YVe5P2yqIpI

### Facing

Tool: 3FL D12
Speed: 13k rpm
SFM: 489 m/min
Chip load: 0.02564 mm/tooth
Feed: 1000 mm/min
Cut: DOC 0.15mm WOC 8mm (66%)

## 2D profile

Tool: 1FL D5
Speed: 18k rpm
SFM: 282 m/min
Chip load: 0.0667 mm/tooth
Feed: 1200 mm/min
Cut: DOC 2.3mm WOC 2mm (40%)

## Circular Pocket - Bore

Tool: FL1 D5
Speed: 18.000k rpm
SFM: 282 m/min
Chip load: 0.066 mm/tooth
Feed: 1200 mm/min
Ramp 3 deg
Ramp feed 600 mm/min
Cut: DOC X.X mm WOC X.X mm (XX %)

## 2D Adaptive

Tool: FL1 D5
Speed: 18.000k rpm
SFM: 282 m/min
Chip load: 0.XXX mm/tooth
Feed: 1200 mm/min
Cut: DOC 3.3 mm WOC 0.8 mm (16 %)

## 2D profile (Finishing)

Tool: 1FL D5
Speed: 18k rpm
SFM: 282 m/min
Chip load: 0.0667 mm/tooth
Feed: 1200 mm/min
Cut: DOC 6.0mm WOC 0.2mm (4%)

## 2D profile (Chamfer)

Tool: 3FL D6
Speed: 13k rpm
SFM: 282 m/min
Chip load: 0. mm/tooth
Feed: 1300 mm/min
Cut: DOC 6.0mm WOC 0.2mm (4%)

# [CNC Basecamp E017: Aluminum Milling Basics](https://www.youtube.com/watch?v=BgQ8USf7FWM)

* Use a single flute bit designed specifically for aluminum.
* Use a bit with the shortest LOC (length of cut) for cutting depth you are dealing with.
  Short bits are more rigid, giving better results and reducing breakage.
* Material lost to the kerf can really add up when cutting many parts side-by-side,
  so consider using 3mm bit rather than a 6 mm bit and get more parts out of your material.
  The smaller bits are also easier for small hobby machines with limited rigidity and power to handle.
* Take light cuts of no more than 0.508 - 0.762 mm per pass. Cutting dense materials like metal requires different
  parameters than cutting wood.
* Use a feed rate of:
    * 889-1143 mm/min for 3mm bits
    * 1143-1524 mm/min for 4-6mm bits.
* Heat kills router bits. Period. Use lubrication to reduce friction and reduce heat. I like to use aerosol cans of
  lubricant specifically for milling.
* Keep the cutting kerf clean using vacuum or compressed air. Chip build-up breaks bits, and those bits are expensive!
  Be careful using compressed air and wear safety glasses.

|               | Value         | Comment |
|---------------|---------------|---------|
| Tool D [mm]   | 6             |         |
| Tool F        | 1             |         |
| S [rpm]       | 17,500        |         |
| F [mm/min]    | 1,143-1,524   |         |
| DOC [mm]      | 0.508 - 0.762 |         |
| WOC [mm]      | 100% - 0.25%  |         |

# 2D adaptive [Shapeoko Feeds & Speeds and Machining Tips!](https://www.youtube.com/watch?v=b8CndwnfoCM)

Tool: FL1 D6
Speed: 25.000k rpm
SFM: XXX m/min
Chip load: 0.XXX mm/tooth
Feed: XXXX mm/min
Cut: DOC X.X mm WOC X.X mm (XX %)

Summary:
Chip Load
tool > 1.587 mm use > 0.0254 mm/tooth
tool < 1.587 mm use micro settings - producer specs

## Template

Tool: FLX DX
Speed: XX.000k rpm
SFM: XXX m/min
Chip load: 0.XXX mm/tooth
Feed: XXXX mm/min
Cut: DOC X.X mm WOC X.X mm (XX %)
