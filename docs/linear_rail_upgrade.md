# Component Selection

## X Axis

* HGR20 sliders x4
* HGR20 rails 500mm x2
* Back Bean:
  * 150 mm width
  * 500mm length
  * thickness: 15mm solid, 20..40mm extrusion?

## Y Axis

HGR20 sliders x4
HGR20 rails 700mm x2

## Z Axis

* Linear Stage 200mm??
* 80mm spindle holder
* https://www.alibaba.com/product-detail/HLTNC-100mm-2500mm-Stroke-Linear-Guides_1600964300723.html

# Summary

* HGR20 slider x8
* HGR20 rail:
  * 700mm x2
  * 200mm x2
* HGR20 rail rust covers
  * https://www.alibaba.com/product-detail/HLTNC-CNC-linear-guide-dust-cover_1600977268705.html?spm=a2700.shop_pl.41413.6.19cc777d7AxIhI

