# DDCS V4.1 usage procedure

1. Home the machine all axes
2. Use Fixed Probing or Base Tool Height probing to set G53 Z offset
3. Setup WCS (in most cases G54) offset - "zero ... axes"
4. Play
   * M6 T{n} causes the machine to go to G28 (MACH) reference point and dialog pops out with tool height offset probing
   * The H{n} is pre-selected - long press `ENTER` to do the probing
   * 