Circuit modification content:
1: upgrade the power module;
2: The input module is changed from common-cathode circuit to common-anode circuit(Support NPN proximity switch);
3: NAND flash replaced by SPI NAND flash;

FPGA modification content:
1: Limit signal can be configured as HOME signal;

Software modification content:Ver: 2017-10-30-89 Modification:
1.R programming mode, when the radius is too large, the arc segment as a straight line for processing;
2. increase soft arc support
3. Increase British display support;
4. Increase HOME source definition, when positive or negative limit is defined as HOME, HOME direction is set automatically. (This function must be implemented on DDCSV1.2);
5. Increase manual override mode JOG configuration items;
6. Increase handwheel stop mode (non-precision mode);
7. Add U disk partition type definition configuration items, modify the configuration to take effect after the system restarts;

Ver:2018-01-21-91 amendments:
1. Addition of "circular centrifugal acceleration" configuration
2. Add A axis command control under soft arc algorithm

1.2 software is compatible with 1.1(FPGA new features are not supported).

DDCSV1.1 software upgrade, the new parameter defaults to 0, their behavior consistent with the previous software.