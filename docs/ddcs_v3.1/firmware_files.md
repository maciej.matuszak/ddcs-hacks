# DDCS 3.1 files

```
.
├── chs           - Chinese lang params definitions 
├── defprobe.nc   - probe macro for mode 3
├── eng           - English lang params definitions
├── gotoz.nc
├── m30.nc
├── motion.out    - actual firmware package
├── null.nc
├── pause.nc
├── probe.nc      - probe macro for mode 1,2
├── rus           - Russian lang params definitions
├── safez.nc
├── slib.nc       - system library
├──  tc           - ??????? lang params definitions
=== extras added by user
├── T.nc          - Tool change macro for M6 
├── extkey1.nc
├── extkey2.nc
├── M200.nc           - custom G-Code commands macros M{200-9999}.nc
```
