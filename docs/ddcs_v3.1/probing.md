# Probing
 A proces of reconciling various tool lengths with working coordinate Z offset

## Probing in mode 1

In this mode you just need a reference point connected to probe. 
* This should be fixed probe. You could do with floating probe but you better be ready with the probe when machine decide to probe (i.e. tool change).
* It records the height of your probe in machine coordinates
* You will have to find origin of your work piece manually.
### Parameters
* `#68 = 1` - Mode1
* `#69` Tool thickness - controlled by DDCS
* `#71` Initial tool's position: [0,1]
  * 0: current position  
  * 1: fixed position (`#72`, `#73`, `#74`)
* `#72` - Initial probe position on X axis
* `#73` - Initial probe position on Y axis
* `#74` - Initial probe position on Z axis
* `#75` - back-off distance after probe on Z axis
* `#78` - Z axis lifting protection speed - speed at which the back-off movement is done

### Procedure  

1. install tool or indicator in colet. This could be a cutter as long as it has consistent bottom. Indicator with ball at the end is better. 
2. move the tip of the tool to origin point of your workpiece this will become 0,0,0 in your work coordinates system i.e. G54
3. "Zero all Axes" from controller. Button sequence: `2ND` -> `ZERO` -> `ZERO`
4. this resets work coordinates and records MACH to work coordinates offset. It zeros `#69` parameter automatically.
5. if `#71`=0 `current position` then move the indicator above the probing plate. To save time keep it close to plate, probing movement is slow.
6. execute probe procedure. Button sequence: `2ND` -> `PROBE` -> `PROBE`
  * if `#71`=1 `fixed position` DDCS controller will move the tool to initial probe position (`#72`, `#73`, `#74`)
  * to probe it will move slowly down until probe contact is detected
  * once the probe touches it will calculate the MACH to probe plate Z offset and stores it as param `#69`. This can be negative if your plate is below MACH Z=0 plane.
  * it will back off the plate at distance `#75`
  * MACH -> work coordinate system is not adjusted this time but `#69` will be used in  calculations

After consecutive tool changes start with step 5. This time at the end the MACH-> work coordinate offset Z value will be adjusted to match new tool length

<span style="background-color:#f7f694; color:black">NOTE: if you "Zero All axes" it will zero the #69 tool thickness and you will have to start again</span>


# Probing internals

(Procedure from Yt Liu)
1. When the `M101` detects the PROBE signal, the internal program executes:
`#400=#866+#569` temporart #400 = MECH Z + probe thickness
2. After the `M102` instruction we generally add the following (`probe.nc`):
```
#402=#400 - MECH Z + probe thickness
#403=1    
#404=-#870  G53 Z
```
Those are "output variables" of `probe.nc` or `defprobe.nc` 

3. When the execution of the probe file ends (`probe.nc` or `defprobe.nc`), the internal program executes:
```
If(#569==0.0) #569=#404
Else if((#402!=0)&&(#403==1)) #802=#402
```

So initially:
-`#870` -> `#404` -> `#569`  initial "tool thickness"
and subsequently: 
`#866`+`#569` -> `#400` -> `#402` -> `#802` - new G53 -> G54 offset

Data Flow:
![ddcs_probe_procedure.jpg](./ddcs_probe_procedure.jpg)