
It was mentioned earlier that custom m instruction (M200.nc-M9999.nc) is possible.
For example i put G01 G53 X20 Y20 F2000 in M2000.nc and copy it in system folder, then i run test.nc which contains M2000 from udisk, but it does not work. I cannot figure out how to make it work?
On a side note in 111 version, i interchange G53/G153 tunctionality,
G01 G53 X20 Y20 F2000 work at F=2000 without problem, but
G00 G53 X20 Y20 still work at #76 default operation speed and not at #80 G0 speed

In order to execute the custom M command, you need to set #108(M Bus IO command response) to 1.
