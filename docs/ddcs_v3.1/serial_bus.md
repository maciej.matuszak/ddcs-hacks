DDCSV through RS232 protocol bus IO expansion, the current version does not support communication with the serial MPG.

Output data packet 7 bytes, including three parts:
1. Data packet header 2 bytes "0x90 0xeb"
2. Output IO word 4 bytes, 0 bit -31 bit are mapped to `#1032`- `#1063` macro variables
3. the checksum, XOR result of the first 6 bytes

Input data packet 7 bytes, including three parts:
1. Data packet header 2 bytes "0x90 0xeb"
2. Input IO words 4 bytes, 0 bit -31 bit are mapped to `#1000` - `#1031` macro variables
3. the checksum, XOR result of the first 6 bytes

Bus output IO usage:
Change the IO status by assigning the relevant macro IO bus output
Example:
`#1032` = 1; set bus IO channel 0 to logic '1'

Bus input IO signal instructions:
M100 bus input IO instruction
`#1100` Bus input IO channel number
`#1101` Bus input IO expectations value
`#1102` waiting time (ms),if <= 0, infinite waiting for the signal to reach the desired value; if> 0,   if the signal does not reach the expected value within the specified time，paused.

Example:
```
(If channel 2 is logic '1', subsequent codes are executed; otherwise, pauses after 1s.)
# 1100 = 2
# 1101 = 1
# 1102 = 1000
M100
```

more here https://bbs.ddcnc.com/forum.php?mod=viewthread&tid=7&extra&page=2&fbclid=IwAR1k7BShUcygB4NOD8gLc4Xf_EE_ZVkb1UzvNUwaek16g7LbfPHG_8-hCvk