# DDCS V4.1 Concepts

## Chain of coordinates

```shell
MECH 
└── G53 (Bias)
    ├── G54 Work Coordinate System (WCS)
    ├── G55 (WCS)
    ├── G56 (WCS)
    ├── G57 (WCS)
    ├── G58 (WCS)
    └── G59 (WCS)
```

* `MECH` Machine coordinate system - fixed origin
* `G53` Relative to `MECH` includes any tool offset
    * Probing Fixed result in adjustment to G53 Z Offset
    * Base Tool height probing result in adjustment to G53 Z Offset (seems to be equivalent to the above)
* `G54`-`G59` Work coordinate systems each relative to `G53`.
* Offsets are stored in `ddcs4/coord1` file, (array of 54 little endian FLOAT64 numbers)

Parsed example:

|            | X       | Y       | Z       | A     | B     | C     |
|------------|---------|---------|---------|-------|-------|-------|
| Unknown    | 0.000   | 0.000   | 0.000   | 0.000 | 0.000 | 0.000 |
| G54 Offset | 114.254 | 141.329 | -14.117 | 4.400 | 0.000 | 0.000 |
| G55 Offset | 5.100   | 5.200   | 5.300   | 5.400 | 0.000 | 0.000 |
| G56 Offset | 0.000   | 0.000   | 0.000   | 0.000 | 0.000 | 0.000 |
| G57 Offset | 0.000   | 0.000   | 0.000   | 0.000 | 0.000 | 0.000 |
| G58 Offset | 0.000   | 0.000   | 0.000   | 0.000 | 0.000 | 0.000 |
| G59 Offset | 9.100   | 9.200   | 9.300   | 9.400 | 0.000 | 0.000 |
| Unknown    | 0.000   | 0.000   | 0.000   | 0.000 | 0.000 | 0.000 |
| G53 Offset | 0.000   | 0.000   | -30.275 | 0.000 | 0.000 | 0.000 |

* To view/modify the offset:
    * `G53` - Bias Management
    * `G54` - `G59` - Coordinate Management

## Bias Management

* Offset between MAC and G53

## Probing Floating

* Finds Current WCS (`G54`-`G59`) Z offset - TBC

## Probing Vertex

* Finds Current WCS (`G54`-`G59`) XYZ offset - TBC

## Probing Fixed

* Finds G53 Z offset

## Tool Height

* Baseline length - Finds G53 Z offset - equivalent to Fixed probing
