G04P0 ; Finish whatever it is doing
M5 ; Turn off the spindle
IF #571 EQ 0 GOTO 1

#4=#2012-#866  ; move to Z top to be above everything (assuming MACH z=0 is at the top)
G91 G00 Z #4
G04 P0; Wait to finish safe Z
;Find offset in MACH coordinates "Initial probe" - "current"
#1=#572-#864
#2=#573-#865
#3=#574-#866
GOTO 2

N1
; If the #71 is "current position" we do not move - reset the values to zeros
#1=0
#2=0
#3=0

N2
G91 G00 X #1 Y #2 ; Move to to selected XY
G04 P0 ; wait for movement to finish
G91 G00 Z #3 ; Move to selected Z position

M101 ; Start monitoring probe signal

; Probe 100mm at 100 speed to detect the tool setting signal
G91 G01 Z-100 F100

M102 ; Stop monitoring probe signal

G04 P0 ; Pause 0s - wait for movement finish

#402=#400 ; Save the Z-axis zero offset of the coordinate system
#403=1 ; Set the automatic correction coordinate system flag

;Save the tool setting block thickness.
;The previous tool setting block thickness parameter was 0.
;The system will use this variable to correct the tool setting
;block thickness parameter to complete the first tool setting.
#404=-#870

G91 G01 Z #575 F #578 ; back off Z after probing
G04 P0; Make sure we will finish previous move and are safety above probe

