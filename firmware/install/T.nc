;#68 -t2 -s1"Tool Setting function mode " -m9 -min=0.000 -max=3.000 -i0"Disable" -i1"mode 1" -i2"mode 2" -i3"mode 3"
;#69 -t1 -s1"Thickness of tool sensor" -s2"mm" -m9 -min=-99.000 -max=99.000
;#2011 -t1 -s1"Probe feedrate" -s2"mm/min" -m9 -min=10.000 -max=999.000

;#2050 -t2 -s1"Use Floating touch plate on stock" -m9 -min=0.000 -max=1.000 -i0"No (0)" -i1"Yes (1)"
;#2051 -t1 -s1"Floating touch plate thickness" -s2"mm" -m9 -min=0.000 -max=999.000

;#572 -t1 -s1"Fixed plate pos. MACH X" -s2"mm" -m9 -min=-999.000 -max=999.000
;#573 -t1 -s1"Fixed plate pos. MACH Y" -s2"mm" -m9 -min=-999.000 -max=999.000
;#574 -t1 -s1"Fixed plate probing start MACH Z" -s2"mm" -m9 -min=-999.000 -max=999.000

;#2055 -t1 -s1"Probe / M6 travel heigth MACH Z" -s2"mm" -m9 -min=-999.000 -max=999.000

;#2056 -t1 -s1"Manual tool change pos MACH X" -s2"mm" -m9 -min=-999.000 -max=999.000
;#2057 -t1 -s1"Manual tool change pos MACH Y" -s2"mm" -m9 -min=-999.000 -max=999.000
;#2058 -t1 -s1"Manual tool change pos MACH Z" -s2"mm" -m9 -min=-999.000 -max=999.000

(Stop Spindle)

(Store the MACH start coordinates - will return here after finished)
#1=#864; current MACH X
#2=#865; current MACH Y
#3=#866; current MACH Z
#4=#450; save current G90/G91 status

(Move to mach safe Z)
G153 Z#2055 F#580

(Move to tool change  X,Y)
G153 X#2056 Y#2057 F#580

(Move to tool change  Z)
G153 Z#2058 F#580

(Pause M00 or M110 - display should still have comments from Tn M6)
M110

(User changes tool and presses continue "SART")

(Move to MACH safe Z)
G90 G153 Z#2055 F#580

(Move to fixed plate X,Y)
G153 X#572 Y#573 F#580

(MOVE to probing height start)
G153 Z#574 F#580

(Probe)
M101
G91 G01 Z-100 F#2011
M102
G04 P0;Sync

(Record G53 offsets)
#402=#400; update the Z offset of MACH->G53
#403=1; Set coordinate system automatic correction flag to true
#404=-#870; What is that doing???

IF [[#402!=0]*[#403==1]]==0 GOTO N2
#802=#402; Save the G53 offset
N2

(Move to mach safe Z)
G91
G00 Z[#2055-#866] F#580
G04 P2000; Sync

(Move to recorded start X,Y)
G00 X[#1-#864] Y[#2-#865] F#580

( Z move after changing offset may result in hitting limits)
(G04 P00)
(G00 Z[#3-#866] F#580)

G90
G04 P0; Sync

(restore G90/G91 status)
IF #4==0 GOTO 3
G91
GOTO 4
N3
G90
N4
